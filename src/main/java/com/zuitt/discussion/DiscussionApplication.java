package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Objects;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	private ArrayList<Student> enrolled = new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String check(){
		return "hello world";
	}

	@GetMapping("/hi")
	public String checkhi(@RequestParam(value="name", defaultValue = "moon")String name){
		return String.format("HI %s", name);
	}
	@GetMapping("/friend")
	public String checkfb(@RequestParam(value="name", defaultValue = "moon")String name, @RequestParam(value="friend", defaultValue = "sun")String friend){
		return String.format("HI %s this is my friend %s", name, friend);
	}

	@GetMapping("/hello/{name}")
	public String hello2(@PathVariable("name") String name){
		return String.format("nice to see you %s", name);
	}

	//ACTIVITY____________________________________---
	ArrayList enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "moon")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling %s!", user);
	}
	@GetMapping("/getEnrollees")
	public ArrayList getEnrollees(){
		return enrollees;
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue = "none")String name, @RequestParam(value="age", defaultValue = "0") String age){
		return String.format("Hello %s, my age is %s ", name, age);
	}

	@GetMapping("/courses/{courseid}")
	public String course(@PathVariable("courseid")String course){
		String details;
		String id = course.toLowerCase();
		if (id.equals("java101")){
			details = "Name: JAVA 101\nSchedule: MWF 10:00am to 12:00 nn\nPrice: 3000 php ";
		}
		else if (id.equals("sql101")){
			details = "Name: JAVA 101\nSchedule: MWF 8:00am to 10:00 nn\nPrice: 3000 php ";
		}
		else{
			details = "code not found";
		}
		return details;
	}

	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user", defaultValue = "none")String user, @RequestParam(value="role", defaultValue = "0") String role){
		String message = "role out of range";
		if(role.equals("teacher")){
			message = String.format("Welcome to class portal, Admin %s!", user);
		}
		else if (role.equals("admin")) {
			message = String.format("Thank you for logging in, Teacher %s ", user);
		}
		else if (role.equals("student")){
			message = String.format("Welcome to the class portal, %s ", user);
		}
		else{
			message = String.format("role out of range");
		}


		return message;
	}
	@GetMapping("/register")
	public String register(@RequestParam(value="id", defaultValue = "0") String id, @RequestParam(value="name", defaultValue = "none")String name, @RequestParam(value="course", defaultValue = "0") String course){
		Student student = new Student(name,course,id);
		enrolled.add(student);
		return String.format("%s Your id number is registered on the system!", id);
	}
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id")String id){
		String message = String.format("the id %s is not found in the system ", id);;
		for(Student student: enrolled){
			if(student.getId().equals(id)){
				message = String.format("Welcome back %s Your are enrolled in %s",student.getName(), student.getCourse());
				break;
			}

		}
		return message;
	}

}
